/*
 *  revision.h
 *  EasyPayEMVCradle
 *
 *  Created by Christophe Fontaine on 22/10/10.
 *  Copyright 2010 Ingenico. All rights reserved.
 *
 */

#ifndef __REVISION_H_
#define __REVISION_H_

// ICISPMSvnVersion : Unversioned directory
// ICISPMVersion    : iSMP v5.0

extern NSString * ICISMPSvnVersion;
extern NSString * ICISMPVersion;

extern const unsigned char iSMPVersionString[];
extern const double iSMPVersionNumber;
#endif // __REVISION_H_

