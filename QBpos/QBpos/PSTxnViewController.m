//
//  PSTxnViewController.m
//  QBPosGateClient
//
//  Created by Sreejith S on 06/10/14.
//  Copyright (c) 2014 NightHub. All rights reserved.
//

#import "PSTxnViewController.h"
#import "INGPosWrapperConstants.h"
#import "NHActivityIndicator.h"


@interface PSTxnViewController ()

@end

@implementation PSTxnViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBarHidden = NO;
    posGateTransaction_ = [PosGate sharedPosGate];
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelPressed)];
    self.navigationItem.rightBarButtonItem = item;
    
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *valueStr  = [userDefault objectForKey:LAST_TRANSACTION];
    int value  = 1;
    
    if([valueStr intValue] > 0)
        value = [valueStr intValue] + 1;
    transactionId_.text = [NSString stringWithFormat:@"%d", value];
    
    amount_.text = @"1.0";
    marchant_.text = @"0";
    resultView_.hidden = YES;
    marchant_.inputAccessoryView = toolBar_;
    transactionId_.inputAccessoryView = toolBar_;
    amount_.inputAccessoryView = toolBar_;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [posGateTransaction_ setDelegate:self];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)cancelPressed
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)doTransaction:(id)sender
{
    resultView_.hidden = YES;
    [self dismissKeyBoard:nil];
    [[NHActivityIndicator loadingView] startAnimation];
    [self processTransaction];
}

- (void) processTransaction{

    

    //Removing MerchantId set functionality, assuming, if merchant already configured, no ned to set it again.
    [posGateTransaction_ setMerchant:[[marchant_ text] floatValue]];
    [posGateTransaction_ setTxnType:@"P"];
    [posGateTransaction_ setTxnId:[transactionId_ text]];
    [posGateTransaction_ setAmountPurchase: [NSDecimalNumber decimalNumberWithString:[amount_ text]]];
    [posGateTransaction_ setAmountCash: [NSDecimalNumber decimalNumberWithString:POS_AMOUNT_CASH_DEFAULT]];
    [posGateTransaction_ setAccountType:POS_DEFAULT_BLANK_STRING]; //Left blamk to select it from PINPAD
    [posGateTransaction_ setTippingEnabled: NO];
    [posGateTransaction_ setTrainingMode: NO];
    [posGateTransaction_ setVerificationEnabled:NO]; //Set this parameter to YES to invoke onverify method in signature verification transactions.
    
    //Set default to A
    [posGateTransaction_ setCardCaptureMethod:@"A"];
    [posGateTransaction_ setCardSource: @"A"];
    [posGateTransaction_ setCardUse:@"A"];
    [posGateTransaction_ setPan:POS_DEFAULT_BLANK_STRING]; //Only relevent if TxnType is K
    [posGateTransaction_ setDateExpiry:POS_DEFAULT_BLANK_STRING]; //Only relevent if TxnType is K
    [posGateTransaction_ setEmvAidIcc:POS_DEFAULT_BLANK_STRING]; //Only relevent if TxnType is C
    [posGateTransaction_ setTrack2:POS_DEFAULT_BLANK_STRING]; //Only relevent if TxnType is S
    [posGateTransaction_ setOriginalTxnType:ORIG_TXN_TYPE_PURCHASE]; //Only relevent if TxnType is V
    [posGateTransaction_ setRrn:POS_DEFAULT_BLANK_STRING];
    [posGateTransaction_ setAuthCode:POS_DEFAULT_BLANK_STRING]; //Only relevent if TxnType is V
    [posGateTransaction_ setStan:POS_DEFAULT_ZERO];
    [posGateTransaction_ setCurrencyCode:POS_DEFAULT_BLANK_STRING];
    
    [posGateTransaction_ Transaction];
    [[NHActivityIndicator loadingView] stopAnimation];
}

//Transactionrequest. callback method.
- (void) TransactionComplete{

    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:posGateTransaction_.txnId forKey:LAST_TRANSACTION];
    [userDefault synchronize];
    
    if ([posGateTransaction_.responseCode isEqualToString:CONNECTION_CHANGED]) {
        
        [self performSelector:@selector(updateStatus:) onThread:[NSThread mainThread] withObject:@"terminal Connection changed" waitUntilDone:YES];
        return;
    }
    
    if (posGateTransaction_.success) {
        [self performSelector:@selector(updateStatus:) onThread:[NSThread mainThread] withObject:@"Transaction Completed succesfully:" waitUntilDone:YES];
        return;
    }
    
    [self performSelector:@selector(updateStatus:) onThread:[NSThread mainThread] withObject:@"Transaction Failed. Plesae try again later." waitUntilDone:YES];
    
    [amount_ resignFirstResponder];
    [marchant_ resignFirstResponder];
    [transactionId_ resignFirstResponder];
    
}

- (IBAction)dismissKeyBoard:(id)sender
{
    [amount_ resignFirstResponder];
    [marchant_ resignFirstResponder];
    [transactionId_ resignFirstResponder];
}

- (void)updateStatus:(NSString *)message
{
    resultView_.hidden = NO;
    [self showAlertWithMessage:message];
    message_.text = message;
    sucessLabel_.text = posGateTransaction_.responseCode;
    resonseLabel_.text = posGateTransaction_.responseText;
}

- (void)showAlertWithMessage:(NSString *)message
{
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        // code to be executed on main thread.If you want to run in another thread, create other queue
    });
    
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alertView show];
}

#pragma mark - TextField Delegates
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
