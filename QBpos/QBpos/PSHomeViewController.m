//
//  PSHomeViewController.m
//  QBPosGateClient
//
//  Created by Sreejith S on 06/10/14.
//  Copyright (c) 2014 NightHub. All rights reserved.
//

#import "PSHomeViewController.h"
#import "PSTxnViewController.h"
#import "AppDelegate.h"
#import "INGPosWrapperConstants.h"

@interface PSHomeViewController ()

@end

@implementation PSHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    posgate = [PosGate sharedPosGate];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateStatus];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)connectionCheck:(id)sender
{
    [self updateStatus];
}

- (void)updateStatus
{
    if (posgate.ready)
    {
        [deviceReadyButton_ setBackgroundColor:[UIColor greenColor]];
        [deviceReadyButton_ setTitle:@"Ready" forState:UIControlStateNormal];
    }
    else
    {
        [deviceReadyButton_ setBackgroundColor:[UIColor redColor]];
        [deviceReadyButton_ setTitle:@"Not Ready" forState:UIControlStateNormal];
    }
    
    
    if (posgate.terminalConnected)
    {
        [terminalButton_ setBackgroundColor:[UIColor greenColor]];
        [terminalButton_ setTitle:@"Connected" forState:UIControlStateNormal];
    }
    else
    {
        [terminalButton_ setBackgroundColor:[UIColor redColor]];
        [terminalButton_ setTitle:@"Not Connected" forState:UIControlStateNormal];
    }
}

- (IBAction)goToTxnScreen:(id)sender
{
    PSTxnViewController *txnViewController = [[PSTxnViewController alloc]initWithNibName:@"PSTxnViewController" bundle:nil];
    UINavigationController *nC = [[UINavigationController alloc]initWithRootViewController:txnViewController];
    [[AppDelegate applicationInstance].navController presentViewController:nC animated:YES completion:nil];
}
- (IBAction)getLastTxn:(id)sender
{
    [posgate setDelegate:self];
    [posgate GetLastTransaction];
}

- (void) GetLastTransactionComplete{

    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:posgate.txnId forKey:LAST_TRANSACTION];
    [userDefault synchronize];
    
}
@end
