

#import "NHActivityIndicator.h"
#import "AppDelegate.h"

const float CornerRadius = 5.0f;
const float AlphaValue = 0.8f;

@implementation NHActivityIndicator

static NHActivityIndicator * loadingView;

+(NHActivityIndicator *) loadingView
{
    @synchronized(loadingView)
    {
        if(!loadingView)
        {
            NSArray *nibfiles = [[NSBundle mainBundle]loadNibNamed:@"NHActivityIndicator" owner:nil options:nil];
            loadingView = (NHActivityIndicator *)[nibfiles objectAtIndex:0];
            if (loadingView) {
                [loadingView initialize];
            }
        }
        return loadingView;
    }
}

- (void)initialize
{
    indicatorView.layer.cornerRadius = CornerRadius;
    indicatorView.layer.masksToBounds = YES;
    indicatorView.alpha = AlphaValue;
}

- (void)startAnimation
{
    [activityIndicator startAnimating];
    self.center = [AppDelegate applicationInstance].window.center;
    [AppDelegate applicationInstance].window.userInteractionEnabled = NO;
    [[AppDelegate applicationInstance].window addSubview:loadingView];
}

- (void)stopAnimation
{
    [activityIndicator stopAnimating];
    [AppDelegate applicationInstance].window.userInteractionEnabled = YES;
    [self removeFromSuperview];
}
    


@end
