

#import <UIKit/UIKit.h>

@interface NHActivityIndicator : UIView{
    
    __weak IBOutlet UIActivityIndicatorView *activityIndicator;
    
    __weak IBOutlet UIView *indicatorView;
    
}

+(NHActivityIndicator *)loadingView;
-(void)startAnimation;
-(void)stopAnimation;

@end
