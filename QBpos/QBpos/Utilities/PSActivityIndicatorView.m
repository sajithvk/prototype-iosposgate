//
//  PSActivityIndicatorView.m
//  QBpos
//
//  Created by Sreejith S on 07/10/14.
//  Copyright (c) 2014 Qburst. All rights reserved.
//

#import "PSActivityIndicatorView.h"
#import "AppDelegate.h"
@implementation PSActivityIndicatorView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self initialSetup];
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)initialSetup
{
    self.backgroundColor = [UIColor clearColor];
    UIWebView *webview = [[UIWebView alloc]initWithFrame:self.frame];
    webview.backgroundColor = [UIColor clearColor];
    [self addSubview:webview];
    int width = CGRectGetWidth(self.frame)*2/3;
    int x = CGRectGetMidX(self.frame) - width/2;
    int y = CGRectGetMidY(self.frame) - width/2;
    UIView *subView = [[UIView alloc]initWithFrame:CGRectMake(x, y, width, width)];
    subView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
    _activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    _activityIndicator.center = subView.center;
    [subView addSubview:_activityIndicator];
    [self addSubview:subView];
    
}
- (void)showLoadingIndicator
{
    [_activityIndicator startAnimating];
    [[AppDelegate applicationInstance].window addSubview:self];
}
- (void)dismissLoadingIndicator
{
    [_activityIndicator stopAnimating];
    [self removeFromSuperview];
}
@end
