//
//  PSActivityIndicatorView.h
//  QBpos
//
//  Created by Sreejith S on 07/10/14.
//  Copyright (c) 2014 Qburst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSActivityIndicatorView : UIView
{
    UIActivityIndicatorView *_activityIndicator;
}

- (void)showLoadingIndicator;
- (void)dismissLoadingIndicator;

@end
