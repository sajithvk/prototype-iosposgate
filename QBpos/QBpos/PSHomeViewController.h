//
//  PSHomeViewController.h
//  QBPosGateClient
//
//  Created by Sreejith S on 06/10/14.
//  Copyright (c) 2014 NightHub. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PosGate/PosGate.h>

@interface PSHomeViewController : UIViewController<PosGateDelegate>
{
    IBOutlet UIButton *deviceReadyButton_;
    IBOutlet UIButton *terminalButton_;
    PosGate *posgate;
}
@end
