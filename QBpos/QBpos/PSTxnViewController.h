//
//  PSTxnViewController.h
//  QBPosGateClient
//
//  Created by Sreejith S on 06/10/14.
//  Copyright (c) 2014 NightHub. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface PSTxnViewController : UIViewController<PosGateDelegate>
{
     PosGate* posGateTransaction_;
    IBOutlet UIView *resultView_;
    __weak IBOutlet UITextField *marchant_;
    __weak IBOutlet UITextField *transactionId_;
    __weak IBOutlet UITextField *amount_;
    __weak IBOutlet UILabel *message_;
    __weak IBOutlet UILabel *sucessLabel_;
    __weak IBOutlet UILabel *resonseLabel_;
     IBOutlet UIToolbar *toolBar_;
}
@end
