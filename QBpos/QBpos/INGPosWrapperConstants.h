//
//  INGPosWrapperConstants.h
//  IngogoPOS
//
//  Created by martin on 22/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
/******INITIALIZATION/TRANSACTION DEFAULT VALUES******/

#define TXN_TYPE_BANk_AQUIRER @"A"
#define TXN_TYPE_TMS_UPDATE @"T"
#define ORIG_TXN_TYPE_PURCHASE @"P"
#define TXN_TYPE_PURCHASE @"P"
#define CARD_CAPTURE_TERMINAL @"A"
#define CARD_SOURCE_TERMINAL_CAPTURE @"A"
#define CARD_USE_SINGLE @"S"


#define POS_DEFAULT_BLANK_STRING @""
#define POS_DEFAULT_ZERO 0

/******RESPONSE STATUS VALUES******/
#define CONFIGURATION 1
#define TRANSACTION 2

#define MERCHANT_CONFIGURATION 1
#define TMS_INITIALIZATION 1
#define BANK_INITIALIZATION 2

#define RESPONSE_SUCCESS 1
#define RESPONSE_FAIL 0

#define LAST_TRANSACTION_OPERATION 1
#define LAST_TRANSACTION_INITIATE 2

#define TRANSACTION_OPERATION 1
#define TRANSACTION_SIGNATURE_REQUEST 2

/******RESPONSE KEYS******/

#define STATUS @"status"
#define RESPONSECODE @"responseCode"
#define RESPONSETEXT @"responseText"
#define MERCHANTID @"merchantId"
#define CATID @"catId"
#define CAID @"caId"
#define DATE @"date"
#define TIME @"time"
#define AIIC @"aiic"
#define NII @"nii"
#define LAST_TXN_STATUS @"lastTxnStatus"
#define FAILED_MAX_TIMES @"maximumCount"

#define TXN_ID @"txnId"
#define PURCHASE_AMOUNT @"purchaseAmount"
#define ACCOUNT_TYPE @"accountType" 
#define RRN @"rrn"
#define AUTH_CODE @"authCode"
#define SETTLE_DATE @"dateSettlement"
#define CARD_NAME @"cardName"
#define CARD_TYPE @"cardType"
#define EXPIRY_DATE @"dateExpiry"
#define CVV @"cvv"
#define PAN @"pan"
#define TRACK1 @"track1"
#define TRACK2 @"track2"
#define SYSTEM_TRACE_AUDIT_NUMBER @"Stan"


/******TRANSACTION KEYS******/

#define AMOUNT @"amount"
#define TRANSACTION_TYPE @"transactiotype"
#define TRANSACTION_ID @"transactionid"
#define CARD_CAPTURE @"cardcapture"
#define CARD_SOURCE @"cardsource"
#define CARD_USE @"carduse"

/******NOTIFICATION USERINFO KEY******/

#define OPERATION  @"operation"
#define OPERATION_STATUS  @"operationstatus"
#define OPERATION_RESPONSE  @"response"
#define DEVICE_STATUS @"devicestatus"


/******NOTIFICATION******/
#define POS_MERCHANT_CONFIGURE @"posmerchantconfiguration"
#define POS_INITIALIZATION @"posinitializationproccess"
#define POS_CONFIGURATION_UPDATE @"posconfigurationupdate"
#define POS_TRANSACTION @"postransactionrequest"
#define POS_LAST_TRANSACTION @"posgetlasttransaction"
#define POS_DEVICE_CONNECTION @"posdeviceconnectionchange"
#define POS_PRINT @"posprintprocess"
#define POS_DIAGNOSTICS @"posdiagnostics"
#define POS_LAST_PRINT @"posLastprintprocess"

/******STATUS KEYS******/

#define SUCCESS_CODE @"00"
#define CONNECTION_CHANGED @"F4"

/******RECEIPT TYPES******/

#define RECEIPT_LOGON @"L"
#define RECEIPT_SIGNATURE @"V"
#define RECEIPT_MERCHANT @"M"
#define RECEIPT_CUSTOMER @"C"
#define RECEIPT_AUDIT @"A"
#define RECEIPT_SETTLEMENT @"S"
#define RECEIPT_DUPLICATE @"U"

/******GENERIC******/

#define LAST_TRANSACTION @"lasttransaction"

#define FETCH_LAST_TRANSACTION_MAX_COUNT 10

#define POS_DEFAULT_BLANK_STRING @""
#define POS_DEFAULT_ZERO 0
#define POS_AMOUNT_CASH_DEFAULT @"0.0"