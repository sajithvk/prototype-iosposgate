//
//  AppDelegate.h
//  QBPosGateClient
//
//  Created by Sreejith S on 06/10/14.
//  Copyright (c) 2014 NightHub. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iSMP/iSMP.h>
#import <PosGate/PosGate.h>
#import "PSActivityIndicatorView.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, ICISMPDeviceDelegate, ICAdministrationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navController;

+ (AppDelegate *)applicationInstance;

@end

//Hex Dump category that extends NSData. This interface is implmented inside iSMP library but is private.
@interface NSData ()

-(NSString *)hexDump;
@end
