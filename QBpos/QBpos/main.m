//
//  main.m
//  QBpos
//
//  Created by Sreejith S on 06/10/14.
//  Copyright (c) 2014 Qburst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
