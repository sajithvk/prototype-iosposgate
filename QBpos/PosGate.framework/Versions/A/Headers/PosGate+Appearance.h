//
//  PosGate+Appearance.h
//  PosGate
//
//  By utilising these classes, application developers can customise the look and feel of the
//  PosGate Dialog using the UIAppearance API
//
//  Created by John Petersen on 17/01/2014.
//  Copyright (c) 2014 com.ingenico. All rights reserved.
//

#import <UIKit/UIKit.h>

// The parent container.  Can be used to reduce appearance scope to just the PosGate dialog.
@interface PosGateDialog : UIViewController
@end

// The main (background) view.  Can be used to modify the dialog background colour
@interface PosGateDialogView : UIView
@end

// The base class for the message labels
@interface PosGateDialogLine : UILabel
/* supports named attribues UITextAttributeTextColor, UITextAttributeFont, UITextAttributeTextShadowColor & UITextAttributeTextShadowOffset */
- (void) setTextAttributes:(NSDictionary *)textAttributes UI_APPEARANCE_SELECTOR;
@end

@interface PosGateDialogLine1 : PosGateDialogLine
@end

@interface PosGateDialogLine2 : PosGateDialogLine
@end

// The base class for the buttons, to allow common attributes to be applied to all of them
@interface PosGateDialogButton : UIButton
@end

@interface PosGateDialogButtonOK : PosGateDialogButton
@end

@interface PosGateDialogButtonClear : PosGateDialogButton
@end

@interface PosGateDialogButtonCancel : PosGateDialogButton
@end

@interface PosGateDialogInput : UITextField
/* supports named attribues UITextAttributeTextColor & UITextAttributeFont */
- (void) setTextAttributes:(NSDictionary *)textAttributes UI_APPEARANCE_SELECTOR;
@end

// The toolbar used when acquiring input
@interface PosGateDialogToolbar : UIToolbar
@end

@interface PosGateDialogBarButtonItem : UIBarButtonItem
@end

@interface PosGateDialogBarButtonCancel : PosGateDialogBarButtonItem
@end

@interface PosGateDialogBarButtonDone : PosGateDialogBarButtonItem
@end

@interface PosGateDialogGraphic : UIWebView
@end
